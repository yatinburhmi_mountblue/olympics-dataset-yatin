var athlete = require("../output/athlete.json");
//console.log(athlete);
/********************************************************************************* */
//Q1
const timesHosted = athlete.reduce((acc,item) => {
    if(!acc.hasOwnProperty(item["City"])){
        acc[item["City"]] = {};
        acc[item["City"]]["Games"] = [];
        acc[item["City"]]["Games"].push(item["Games"]);
        acc[item["City"]]["NumberOfTimesHosted"] = 1;
    }
    else{
        if(!acc[item["City"]]["Games"].includes(item["Games"])){
            acc[item["City"]]["Games"].push(item["Games"]);
            acc[item["City"]]["NumberOfTimesHosted"]++;
        }
    }
    return acc;
},{})

var timesHostedArr = Object.entries(times)
console.log(timesHosted);

/*************************************************************************************** */

// Q2
const filteredCountries = athlete.filter((item) => item["Year"] >= 2000 && item["Medal"] !== 'NA' ? item : null);

const mostMedal = filteredCountries.reduce((acc,item)=> {
    if(!acc.hasOwnProperty(item["Team"])){
        acc[item["Team"]] = {};
        acc[item["Team"]][item["Medal"]] = 1;
        acc[item["Team"]]["Total"] = 1;
    }else{
        if(!acc[item["Team"]].hasOwnProperty(item["Medal"])){
            acc[item["Team"]][item["Medal"]] = 1;
        }
        else{
            acc[item["Team"]][item["Medal"]] += 1;
            acc[item["Team"]]["Total"] += 1;
        }
    }
    return acc;
},{})

var mostMedalArr = Object.entries(mostMedal)
// console.log(mostMedalArr);
var topTenMostMedalArr = mostMedalArr.sort((a,b)=>{
    return b[1].Total - a[1].Total;
}
).slice(0,10);
console.log(topTenMostMedalArr);

/**************************************************************************************** */
// Q3

const yearWiseCount = athlete.reduce((acc,item)=>{
    if(!acc.hasOwnProperty(item["Games"])){
      acc[item["Games"]] = {};
      acc[item["Games"]]["id"] = [];
      acc[item["Games"]]["id"].push(item.ID);
      if(item.Sex === 'M'){
        acc[item["Games"]]["MaleCount"] = 1;
        acc[item["Games"]]["FemaleCount"] = 0;
    }else{
        acc[item["Games"]]["MaleCount"] = 0;
        acc[item["Games"]]["FemaleCount"] = 1;
        }
    }else{
         if(acc[item["Games"]]["id"].indexOf(item.ID) === -1){
            acc[item["Games"]]["id"].push(item.ID);
            if(item.Sex === 'M'){
              acc[item["Games"]]["MaleCount"]++;
            }else{
              acc[item["Games"]]["FemaleCount"]++;
            }
          }
        }
    return acc;
  },{});
//console.log(yearWiseCount)

  var yearWiseCountArr = Object.entries(yearWiseCount)

const decadeWiseCount = yearWiseCountArr.reduce((acc,item)=>{
    //console.log(parseInt(item["Year"]));
    let intValYear = parseInt(item[0]);
    let decade = (Math.floor(intValYear/10))*10;
    //console.log(decade);
    if(!acc.hasOwnProperty(decade)){
        acc[decade] = {};
        acc[decade]["M"] = item[1].MaleCount;
        acc[decade]["F"] = item[1].FemaleCount;
    }
    else{
        if(acc[decade].hasOwnProperty("M")){
            acc[decade]["M"] += item[1].MaleCount;
        }
        if(acc[decade].hasOwnProperty("F")){
            acc[decade]["F"] += item[1].FemaleCount;
        }
    }
    return acc;
},{})

console.log(decadeWiseCount);

/**************************************************************************/

// //Q4

var boxingObj = athlete.filter((item) => (item["Event"] === "Boxing Men's Heavyweight") && (item["Age"] !== 'NA')? item:null);

//console.log(boxingObj);

var sumAges = boxingObj.reduce(function(acc,item){
    if(!acc.hasOwnProperty(item["Year"])){
        acc[item["Year"]] = {};
        acc[item["Year"]]["age"] = parseInt(item["Age"]);
        acc[item["Year"]]["count"] = 1;
        }
    else{
        acc[item["Year"]]["age"] += parseInt(item["Age"]);
        acc[item["Year"]]["count"]++;
    }
    return acc;
},{})

console.log(sumAges);
let sumAgesArr = Object.entries(sumAges);
console.log(sumAgesArr);

let avgAge = sumAgesArr.reduce((acc, currVal) => {
    if(!acc.hasOwnProperty(currVal[0])){
        acc[currVal[0]] = currVal[1]["age"]/currVal[1]["count"];
    }
    else{
        acc[currVal[0]] = currVal[1]["age"]/currVal[1]["count"];
    }
    return acc;
},{})

console.log(avgAge);

/********************************************************************************* */

//Q5

const indianPlayersObj = athlete.filter((item) => item["Team"]==="India" && item["Medal"] !== 'NA'?item:null);

const indianMedalWinnerNames = indianPlayersObj.reduce((acc,item) => {
    if(!acc.hasOwnProperty(item["Games"])){
        acc[item["Games"]] = [];
        acc[item["Games"]].push(item["Name"]);
    }
    else{
        acc[item["Games"]].push(item["Name"]);
    }
    return acc;
},{})
console.log(indianMedalWinnerNames);

/********************************************************************************************* */
